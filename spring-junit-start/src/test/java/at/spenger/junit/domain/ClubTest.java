package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;

import org.junit.Test;

import at.spenger.junit.domain.Person.Sex;

public class ClubTest {
	
	@Test
	public void testEnter() {
		 Club c = new Club();
		 Person p = new Person("Hans", "Peter", LocalDate.now(), Sex.MALE );
		 boolean erg = c.enter(p);
		 boolean erwartet = true;
		 assertEquals(erwartet, erg);
	}

	@Test
	public void testNumberOf() {
		
		Club c = new Club();
		 
		int erg = c.numberOf();
		int erwartet = 0;
		assertEquals(erwartet, erg);
	}
		
	@Test
	public void testAverageAge() {
		
		Club c = new Club();
		Person p1 = new Person("Hans", "Peter", LocalDate.now().minusYears(20), Sex.MALE );
		Person p2 = new Person("Heinz", "Peter", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		
		c.enter(p1);
		c.enter(p2);
				
		double erg = c.averageAge();
		double erwartet = 14.5;
		assertEquals(erwartet, erg, 0.0000000001); 
		// anscheinend werden Doubles obwohl sie den gleichen Wert haben 
		// nicht mit gleichem Wert gespeichert 
		// assertEquals(erwartet, erg); => 14.5 ist nicht 14.5
	
	}
	
	@Test
	public void testSortByName() {
		Club c = new Club();
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(20), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(20), Sex.MALE );
		
		c.enter(p1);
		c.enter(p2);
		c.enter(p3);
		
		Club check = new Club();
		
		check.enter(p2);
		check.enter(p3);
		check.enter(p1);
		
		List<Person> erg = c.sortByName();
		List<Person> erwartet = check.getPersons();
		
		assertEquals(erwartet, erg);
	}
	
	//Austreten
	@Test
	public void testRemove() {
		Club c = new Club();
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(20), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(20), Sex.MALE );
		
		c.enter(p1);
		c.enter(p2);
		c.enter(p3);
		
		Club check = new Club();
		
		check.enter(p1);
		check.enter(p3);
		
		List<Person> erg = c.remove(1);
		List<Person> erwartet = check.getPersons();
		
		assertEquals(erwartet, erg);
		
	}
	//2 weitere
	@Test
	public void testOldestPerson() {
		Club c = new Club();
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(20), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(20).minusDays(5), Sex.MALE );
		
		c.enter(p1);
		c.enter(p2);
		c.enter(p3);

		Person erwartet = p3;
		Person erg = c.oldestPerson();
				
		assertEquals(erwartet, erg);
	} 
	
	@Test
	public void testYoungestPerson() {
		Club c = new Club();
		Person p1 = new Person("Karl", "Quop", LocalDate.now().minusYears(10).plusMonths(3).minusDays(15), Sex.MALE );
		Person p2 = new Person("Heinz", "Ahorn", LocalDate.now().minusYears(10).plusMonths(3).minusDays(10), Sex.MALE );
		Person p3 = new Person("Hans", "Peter", LocalDate.now().minusYears(10).plusMonths(3).minusDays(20), Sex.MALE );

		c.enter(p1);
		c.enter(p2);
		c.enter(p3);

		Person erwartet = p2;
		Person erg = c.youngestPerson();
				
		assertEquals(erwartet, erg);
		
	}
}
