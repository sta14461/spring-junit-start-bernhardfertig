package at.spenger.junit.domain;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Club {
	private List<Person> l;
	
	public Club() {
		l = new ArrayList<>();
	}
	
	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}
	
	public int numberOf() {
		return l.size();
	}
	
	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}

	public double averageAge() {
	
		if(numberOf() != 0) {
			double d = 0;
			for(Person p : l) {
				d += Period.between(p.getBirthday(), LocalDate.now()).getYears();
			}
			System.out.println( d / numberOf() );
			return d / numberOf();
		}
		return 0;
	}
	
	public List<Person> sortByName() {
		if(numberOf() != 0){
			Collections.sort(l);
		}
		return getPersons();
	}
	 
	public List<Person> remove(int index) {
		if(numberOf() != 0 && index < numberOf()) {
			l.remove(index);
			return Collections.unmodifiableList(l);
		}
		return null;
	}
	
	public Person oldestPerson() {
		
		if(numberOf() != 0) {
			Collections.sort(l, new AgeComparator());
			return l.get(0);
		}
		return null;
	}
	
	public Person youngestPerson() {
		if(numberOf() != 0) {
			Collections.sort(l, new AgeComparator());
			Collections.reverse(l); //da der AgeComparator den �ltesten zuerst und den J�ngsten zuletzt in die Liste ordnet wird durch reverse die Reihenfolge umgedreht
			return l.get(0);
		}
		return null;
	}
}
