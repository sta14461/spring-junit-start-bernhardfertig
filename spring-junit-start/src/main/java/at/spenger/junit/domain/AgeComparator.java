package at.spenger.junit.domain;

import java.time.Period;
import java.util.Comparator;

public class AgeComparator implements Comparator<Person>{
 
	@Override
	public int compare(Person p, Person p1) {
		
		if(Period.between(p.getBirthday(), p1.getBirthday()).getYears() > 0) {
			return -1;
		}
		if(Period.between(p.getBirthday(), p1.getBirthday()).getYears() < 0) {
			return 1;
		}
		else {
			if(Period.between(p.getBirthday(), p1.getBirthday()).getMonths() > 0) {
				return -1;
			}
			if(Period.between(p.getBirthday(), p1.getBirthday()).getMonths() < 0) {
				return 1;
			}
			else {
				if(Period.between(p.getBirthday(), p1.getBirthday()).getDays() > 0) {
					return -1;
				}
				if(Period.between(p.getBirthday(), p1.getBirthday()).getMonths() < 0) {
					return 1;
				}
			}
		}
		return 0;
	}

}
